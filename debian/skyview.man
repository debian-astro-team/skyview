.TH SKYVIEW "1" "17 Dec 2016"
.SH NAME
SkyView \- Image generation from a range of remote databases
.SH SYNOPSIS
.B skyview
\fISurvey=\fRarg [\fIkey=\fRarg ...]
.br
.SH DESCRIPTION
SkyView provides users with a local SkyView system on their own
machines.  Users can generate FITS, GIF, JPEG, ... images from major
surveys in any requested geometry, resample and mosaic their own data,
overlay grids and catalog positions, and create their own surveys.
.SH OPTIONS
Keys are not case sensitive but values may be. Values specifying more
than one value use a comma as a separator.
.BR
Valid keys include:
.TP
.BR Survey
A survey for which an image is requested (e.g, WENSS, DSS)
(Required, with no default.) To request multiple surveys, separate
them by commas. Survey names are not case sensitive.
The 'user' survey references data given in the UserFile argument
.TP
.BR Settings
A list of one or more files from which settings are read.
File settings are processed after argument settings.
.SS Keys that define the geometry of the output image.
.TP
.BR Position
The longitude and latitude of the center of the image.  This may be
given as coordinates or as a target name [No default]
.TP
.BR Lon
The decimal longitude of the center of the image [No default]
.TP
.BR Lat
The decimal latitude of the center of the image [No default]
.PP
Either Position or Lon and Lat must be specified.
.TP
.B Coordinates
A string giving the coordinate system and epoch (if required)  used,
e.g., J, J2000, B1950, ICRS, Gal, H2010.438 [J]
.TP
.B Equinox
Epoch of equinox of coordinates [2000]
.TP
.B Projection
The three letter FITS WCS specification of the projection. [Tan]
.TP
.B Scale:
The pixels scale in degrees (may be one or two values) [survey dependent]
.TP
.B Size
The size of the entire image in degrees [scale*pixels]
.PP
Generally you specify either scale or size but not both.
.TP
.B Pixels
The number of pixels in the image (may be one or two values) [300]
.TP
.B Rotation
A rotation angle in the projection plane [0]
.SS Keys that control optional graphical outputs

.TP
.B Quicklook
The format desired for quicklook output.  JPEG, GIF, BMP and TIFF are
supported.  If another graphic keyword is used without specifying the
format it defaults to JPEG.
.TP
.B Scaling
How the output pixel intensities are to be scaled. Linear, Log, Sqrt,
HistEq are supported [Linear]
.TP
.B Min
Set all smaller pixel values to this value before scaling.
.TP
.B Max
Set all larger pixel values to this value before scaling.
.TP
.B RGB
Create a three color overlay
.TP
.B RGBSmooth
Box smoothing for planes of RGBImages
.TP
.B Invert
Invert the color table response (or gray scale if no color lookup
table is defined).
.TP
.B LUT
Load a color lookup table.  See the users guide for lists of internal
color tables and files supplied in the JAR.  User generated color
tables may also be used.
.TP
.B Grid
Plot a grid of the image or specified coordinate system as an image
overlay.
.TP
.B GridLabels
Label the grid lines with the coordinate entries
.TP
.B PlotScale
Scaling for plot symbols
.TP
.B PlotFontSize
Size of plot text in points.
.TP
.B PlotColor
Color of plot graphics
.SS Catalog query keys
.TP
.B Catalog
The names of the catalogs to be queried, or URLs that specify VOTable
outputs.
.TP
.B CatalogFile
The name of a file in which to print the IDs, positions, and pixel
locations of the catalog entries found.
.TP
.B CatalogRadius
A maximum radius from the center of the image for which catalog
entries are desired.
.TP
.B CatalogQualifier
A qualification on a column of the returned table (e.g. vmag<10)
.TP
.B CatalogIDs
When plotting catalog positions print the numeric ID of the catalog
entry
.SS Keys that define the sampling used
.TP
.B Sampler
The sampling algorithm to be used.
NN, LI, Lanczos[n], Spline[n], Clip [NN]
.TP
.B Ebins
Specification of sampling in energy dimension: e0,de,ne
[survey dependent for 3-d surveys]
 e0 is the starting offset of the first output bin.
 de is the width of the output bins (relative to the input).
 n  is th number of output bins.
.SS Keys that set characteristics of the output FITS file
.TP
.B Output
The output file stem [output]
.TP
.B Compress
If specified the output is gzip compressed
.TP
.B Float
If specified the output is 4 byte rather than 8 byte real
.TP
.B NoFITS
A FITS file should not be produced.
.SS Smoothing
.TP
.B Smooth
A box size (or sizes for non-square box) for a box car smoothing.
.SS Contours
.TP
.B Contour
Create a contour map overlay
.TP
.B ContourSmooth
Smoothing to be applied to survey before contouring
.SS Keys that set where to look for survey and store survey data.
.TP
.B XMLRoot
A directory of XML files that contain survey descriptions
.TP
.B SurveyManifest
Overrides the name of the resource used to look for the survey
manifest (normally included in JAR).
.TP
.B SurveyXML
A file or files giving XML descriptions of surveys
.TP
.B UserFile
A file (or list of files) that defines the 'User' survey.  These are
the actual survey images not survey metadata.
.TP
.B Cache
A directory (or comma delimited set of directories) where cached files
are to be found.  The first cache directory is where survey files used
in this request will be saved.
.TP
.B PurgeCache
Delete any survey files downloaded in this request.
.SS Keys that define the classes used for processing the request.
.TP
.B Deedger
Class that eliminates transitions at input image boundaries.
.TP
.B SurveyFinder
Class that loads and finds surveys given survey names/IDs
.TP
.B ImageFinder
Class that finds images in a survey to use in a mosaic.
.TP
.B PreProcessor
Class[es] to process input images before resampling.
.TP
.B Mosaicker
Class that actually mosaics survey data to create output image.
.TP
.B PostProcessor
Class[es] that process the output image after mosaicking.
.SS Keys that change the behavior of the process.
.TP
.B ImageJ
Leave an ImageJ session running with the generated images when finished.
.SH Available surveys (including all aliases)

  0035mhz, 0408mhz, 1420mhz, 1420mhz (bonn), 2mass-h
  2mass-j, 2mass-k, 2massh, 2massj, 2massk, 35mhz, 408mhz
  4850mhz, 4mass, akari n160, akari n60, akari wide-l
  akari wide-s, akari-n160, akari-n60, akari-wide-l
  akari-wide-s, akari_n160, akari_n60, akari_wide-l
  akari_wide-s, bat flux 1, bat flux 100-150, bat flux 14-20
  bat flux 150-195, bat flux 2, bat flux 20-24, bat flux 24-35
  bat flux 3, bat flux 35-50, bat flux 4, bat flux 5
  bat flux 50-75, bat flux 6, bat flux 7, bat flux 75-100
  bat flux 8, bat snr 1, bat snr 100-150, bat snr 14-195
  bat snr 14-20, bat snr 150-195, bat snr 2, bat snr 20-24
  bat snr 24-35, bat snr 3, bat snr 35-50, bat snr 4
  bat snr 5, bat snr 50-75, bat snr 6, bat snr 7, bat snr 75-100
  bat snr 8, bat snr sum, bat-flux-1, bat-flux-2, bat-flux-3
  bat-flux-4, bat-flux-5, bat-flux-6, bat-flux-7, bat-flux-8
  bat-snr-1, bat-snr-2, bat-snr-3, bat-snr-4, bat-snr-5
  bat-snr-6, bat-snr-7, bat-snr-8, bat-snr-sum, cdfs less
  cdfs: less, cgro comptel, co, co2d, cobe dirbe/aam
  cobe dirbe/zsma, cobeaam, cobezsma, comptel, digitized sky survey
  dss, dss1 blue, dss1 red, dss1b, dss1r, dss2 blue
  dss2 ir, dss2 red, dss2b, dss2ir, dss2r, dssold, ebhis
  ebhis(nhi), egret (3d), egret <100 mev, egret >100 mev
  egret(3d), egret1000, egret30, egret3d, egrethard
  egretsoft, euve 171 a, euve 405 a, euve 555 a, euve 83 a
  euve171, euve405, euve555, euve83, fermi 1, fermi 2
  fermi 3, fermi 4, fermi 5, fermi band 1, fermi band 2
  fermi band 3, fermi band 4, fermi band 5, fermi1, fermi2
  fermi3, fermi4, fermi5, first, galex far uv, galex near uv
  galexfar, galexnear, gb6, gb6 (4850mhz), gb6/pmn, geetee
  gns, goods acis fb, goods acis hb, goods acis sb, goods acs b
  goods acs i, goods acs v, goods acs z, goods herschel 100
  goods herschel 160, goods herschel 250, goods herschel 350
  goods herschel 500, goods irac 1, goods irac 2, goods irac 3
  goods irac 3.6, goods irac 4, goods irac 4.5, goods irac 5.8
  goods irac 8.0, goods isaac h, goods isaac j, goods isaac ks
  goods mips, goods n vla, goods vimos r, goods vimos u
  goods: chandra acis fb, goods: chandra acis hb, goods: chandra acis sb
  goods: herschel 100, goods: herschel 160, goods: herschel 250
  goods: herschel 350, goods: herschel 500, goods: hst acs b
  goods: hst acs i, goods: hst acs v, goods: hst acs z
  goods: hst nicmos, goods: spitzer irac 3.6, goods: spitzer irac 4.5
  goods: spitzer irac 5.8, goods: spitzer irac 8.0, goods: spitzer mips
  goods: spitzer mips 24, goods: vla north, goods: vlt isaac h
  goods: vlt isaac j, goods: vlt isaac ks, goods: vlt vimos r
  goods: vlt vimos u, goods_acs_b, goods_acs_i, goods_acs_v
  goods_acs_z, goodsacisfb, goodsacishb, goodsacissb
  goodsherschel1, goodsherschel2, goodsherschel3, goodsherschel4
  goodsherschel5, goodsirac 1, goodsirac 2, goodsirac 3
  goodsirac 4, goodsmips, goodsnicmos, goodsnvla, granat/sigma
  granat/sigma flux, granat/sigma sig, granat_sigma_flux
  granat_sigma_sig, gtee, gtee 35mhz, h-alpha, h-alpha comp
  h-alpha composite, halpha, halpha/comp, hawaii hdf b
  hawaii hdf i, hawaii hdf r, hawaii hdf u, hawaii hdf v0201
  hawaii hdf v0401, hawaii hdf z, hawaii: hdf b, hawaii: hdf i
  hawaii: hdf r, hawaii: hdf u, hawaii: hdf v0201, hawaii: hdf v0401
  hawaii: hdf z, heao 1 a-2, heao1a, hi map, hi4pi, hi4pi(nhi)
  hri, hriint, hudf: vlt isaac ks, hudfisaac, int gal 17-35 exp
  int gal 17-35 flux, int gal 17-35 sig, int gal 17-60 exp
  int gal 17-60 flux, int gal 17-60 sig, int gal 35-80 exp
  int gal 35-80 flux, int gal 35-80 sig, integral/spi gc
  integralspi_gc, intgal1735e, intgal1735f, intgal1735s
  intgal1760e, intgal1760f, intgal1760s, intgal3580e
  intgal3580f, intgal3580s, iras  100, iras  12, iras  25
  iras  60, iras 100, iras 100 micron, iras 12, iras 12 micron
  iras 25, iras 25 micron, iras 60, iras 60 micron, iras100
  iras12, iras25, iras60, iris  100, iris  12, iris  25
  iris  60, iris 100, iris 100 micron, iris 12, iris 12 micron
  iris 25, iris 25 micron, iris 60, iris 60 micron, iris100
  iris12, iris25, iris60, mell-b, mell-g, mell-r, mellinger blue
  mellinger green, mellinger red, mellinger-b, mellinger-g
  mellinger-r, n-vss, neat, neat/skymorph, nh, nvss
  planck 030, planck 044, planck 070, planck 100, planck 143
  planck 217, planck 353, planck 545, planck 857, planck-030
  planck-044, planck-070, planck-100, planck-143, planck-217
  planck-353, planck-545, planck-857, planck030, planck044
  planck070, planck100, planck143, planck217, planck353
  planck545, planck857, pmn, pspc 0.6 deg-cnt, pspc 0.6 deg-counts
  pspc 0.6 deg-exp, pspc 0.6 deg-expos, pspc 0.6 deg-int
  pspc 0.6 deg-inten, pspc 1.0 deg-cnt, pspc 1.0 deg-counts
  pspc 1.0 deg-exp, pspc 1.0 deg-expos, pspc 1.0 deg-int
  pspc 1.0 deg-inten, pspc 2.0 deg-cnt, pspc 2.0 deg-counts
  pspc 2.0 deg-exp, pspc 2.0 deg-expos, pspc 2.0 deg-int
  pspc 2.0 deg-inten, pspc0.6cnt, pspc0.6exp, pspc0.6int
  pspc1cnt, pspc1exp, pspc1int, pspc2cnt, pspc2exp, pspc2int
  rass, rass background 1, rass background 2, rass background 3
  rass background 4, rass background 5, rass background 6
  rass background 7, rass broad, rass hard, rass soft
  rass-cnt broad, rass-cnt hard, rass-cnt soft, rass-int broad
  rass-int hard, rass-int soft, rass3bb, rass3hb, rass3sb
  rassback1, rassback2, rassback3, rassback4, rassback5
  rassback6, rassback7, rassbb, rassbbc, rassbbi, rassbck1
  rassbck2, rassbck3, rassbck4, rassbck5, rassbck6, rassbck7
  rasscnt bb, rasscnt hb, rasscnt sb, rasshb, rasshbc
  rasshbi, rassint bb, rassint hb, rassint sb, rasssb
  rasssbc, rasssbi, rosat wfc f1, rosat wfc f2, rxte allsky 3-20kev
  rxte allsky 3-20kev flux, rxte allsky 3-20kev sig
  rxte allsky 3-8kev, rxte allsky 3-8kev flux, rxte allsky 3-8kev sig
  rxte allsky 8-20kev, rxte allsky 8-20kev flux, rxte allsky 8-20kev sig
  rxte3_20k_flux, rxte3_20k_sig, rxte3_8k_flux, rxte3_8k_sig
  rxte8_20k_flux, rxte8_20k_sig, sdss g, sdss i, sdss r
  sdss u, sdss z, sdssdr7 g, sdssdr7 i, sdssdr7 r, sdssdr7 u
  sdssdr7 z, sdssdr7g, sdssdr7i, sdssdr7r, sdssdr7u
  sdssdr7z, sdssg, sdssi, sdssr, sdssu, sdssz, sfd 100 micron
  sfd 100m, sfd dust, sfd dust map, sfd100m, sfddust
  shassa c, shassa cc, shassa h, shassa sm, shassa-c
  shassa-cc, shassa-h, shassa-sm, shassa_c, shassa_cc
  shassa_h, shassa_sm, stripe 82 vla, stripe82vla, sumss
  sumss 843 mhz, tgss, tgss adr1, tgss_adr1, ukidss h
  ukidss j, ukidss k, ukidss y, ukidss-h, ukidss-j, ukidss-k
  ukidss-y, ukidssh, ukidssj, ukidssk, ukidssy, vla first (1.4 ghz)
  vlss, vlssr, w-nss, wenss, wfcf1, wfcf2, wise 12, wise 22
  wise 3.4, wise 4.6, wise w1, wise w2, wise w3, wise w4
  wise12, wise22, wise3.4, wise4.6, wisew1, wisew2, wisew3
  wisew4, wmap, wmap ilc, wmap k, wmap ka, wmap q, wmap v
  wmap w, wmap-k, wmap-ka, wmap-q, wmap-v, wmap-w, wmapilc
  wmapk, wmapka, wmapq, wmapv, wmapw
