/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package skyview.survey;

import java.util.ArrayList;
import skyview.geometry.CoordinateSystem;

/**
 *
 * @author tmcglynn
 */
public class AkariGenerator extends SIAPGenerator {
    
    /** It appears that the SIAP query for AKARI is assumed to request data
     * in ecliptic coordinates.  So we'll convert the input RA/Dec to ECL
     * and then get the images.  The AKARI output
     * fully describes the proxy image, so we don't need to pass anything
     * else special.
     * @param ra
     * @param dec
     * @param size
     * @param spells 
     */
    
    public void getImages(double ra, double dec, double size, ArrayList<String> spells) throws Exception {
        CoordinateSystem ecl = CoordinateSystem.factory("E2000");
        double[] junit = skyview.geometry.Util.unit(new double[]{Math.toRadians(ra),Math.toRadians(dec)});
        double[] eunit = ecl.getRotater().transform(junit);
        double[] ecoords = skyview.geometry.Util.coord(eunit);
        ra = Math.toDegrees(ecoords[0]);
        dec = Math.toDegrees(ecoords[1]);
        super.getImages(ra, dec, size, spells);               
    }
}
