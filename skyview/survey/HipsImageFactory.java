package skyview.survey;

import skyview.executive.Settings;

/** A little class that creates a HiPS image from the spell provided. */
public class HipsImageFactory implements ImageFactory {
        
    /** Create a HiPS using the spell.
     *   
     * @param spell  A string encoding the information to create the HiPS in
     *               the form hips_directory|scale
     *               where the | is used to separate the elements.
     * @return 
     */    
    public HipsImage factory(String spell) {
	
	try {
            String[] fields = spell.split("\\|");
            if (fields.length != 2) {
                throw new IllegalArgumentException("Illegal spell:"+spell+" for HiPS");
            }
            
	    HipsImage img = new HipsImage(fields[0]);
            String[] sz = Settings.getArray("size");
            String[] pixels = Settings.getArray("pixels");
            double scale = Double.parseDouble(sz[0])/Double.parseDouble(pixels[0]);
            img.initialize(scale);
            return img;
            
	} catch (Exception e) {
	    e.printStackTrace();
	    throw new Error("Irrecoverable error for HiPS: "+spell);
	}
    }
}
