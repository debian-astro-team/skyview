/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package skyview.tile;

/**
 *
 * @author tmcglynn
 */
public class TileCenters {
    
    public static void main(String[] args) {
        if (args.length < 1) {
            System.err.println("Usage: TileCenters delta [dmin dmax]]");
        }
        
        double delta = Double.parseDouble(args[0]);
        double dmin = -90;
        double dmax =  90;
        if (args.length > 1) {
            dmin = Double.parseDouble(args[1]);
            if (args.length > 2) {
                dmax = Double.parseDouble(args[2]);
            }
        }
        double dec = dmin;
        while (dec <= dmax) {
            // Make a big larger than needed.
            double dn = 440*Math.cos(Math.toRadians(dec))/delta;
            if (dn < 1) {
                dn = 1;
            }
            dn = Math.ceil(dn);
            double dra = 360/dn;
            for (int i=0; i<dn; i += 1) {
                System.out.printf("%10.3f %10.3f\n", dra*i,dec);
            }
            dec += delta;
        }        
    }    
}
